package ru.trifonov.tm.view;

import ru.trifonov.tm.service.impl.ProjectServiceImpl;
import ru.trifonov.tm.service.impl.TaskServiceImpl;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String command;
        String name;
        String idProject;
        String idTask;

        ProjectServiceImpl projectService = new ProjectServiceImpl();
        TaskServiceImpl taskService = new TaskServiceImpl();
        Scanner inCommand = new Scanner(System.in);

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Enter \"help\" for show all commands. ");

        do {
            command = inCommand.nextLine();
            switch (command) {
                case (CommandForProjectManager.H):
                case (CommandForProjectManager.HELP):
                    System.out.println("help: Show all commands. " +
                            "\n project-create (pc): Create new project." +
                            "\n project-update (pu): Update selected project." +
                            "\n project-list (pl): Show all projects." +
                            "\n project-remove (pr): Remove selected project." +
                            "\n project-clear (pcl): Remove all projects." +
                            "\n" +
                            "\n task-create (tc): Create new task." +
                            "\n task-update (tu): Update selected task." +
                            "\n task-list (tl): Show all tasks." +
                            "\n task-remove (tr): Remove selected task." +
                            "\n task-clear (tcl): Remove all tasks." +
                            "\n " +
                            "\n exit: Exit from app.");
                    break;

                    /*Projects commands*/

                case (CommandForProjectManager.PC):
                case (CommandForProjectManager.PROJECT_CREATE):
                    System.out.println("[PROJECT CREATE]");
                    System.out.println("Enter name");
                    name = inCommand.nextLine();
                    projectService.create(name, projectService.generateRandomBasedUUID());
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.PU):
                case (CommandForProjectManager.PROJECT_UPDATE):
                    System.out.println("PROJECT UPDATE");
                    System.out.println("Enter the ID of the project you want to update");
                    idProject = inCommand.nextLine();
                    System.out.println("Enter new name");
                    name = inCommand.nextLine();
                    projectService.update(name, idProject);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.PL):
                case (CommandForProjectManager.PROJECT_LIST):
                    System.out.println("[ALL PROJECT LIST]");
                    projectService.read();
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.PR):
                case (CommandForProjectManager.PROJECT_REMOVE):
                    System.out.println("[PROJECT REMOVE]");
                    System.out.println("Enter the ID of the project you want to delete");
                    idProject = inCommand.nextLine();
                    projectService.delete(idProject);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.PCL):
                case (CommandForProjectManager.PROJECT_CLEAR):
                    System.out.println("[ALL PROJECT CLEAR]");
                    projectService.clear();
                    System.out.println("[OK]");
                    break;

                    /*Task commands*/

                case (CommandForProjectManager.TC):
                case (CommandForProjectManager.TASK_CREATE):
                    System.out.println("[TASK CREATE]");
                    System.out.println("Enter ID of project");
                    idProject = inCommand.nextLine();
                    System.out.println("Enter name task");
                    name = inCommand.nextLine();
                    taskService.create(name, taskService.generateRandomBasedUUID(), idProject);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.TU):
                case (CommandForProjectManager.TASK_UPDATE):
                    System.out.println("[TASK UPDATE]");
                    System.out.println("Enter the ID of the task you want to update");
                    idTask = inCommand.nextLine();
                    System.out.println("Enter new name");
                    name = inCommand.nextLine();
                    taskService.update(name, idTask);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.TL):
                case (CommandForProjectManager.TASK_LIST):
                    System.out.println("[TASK LIST]");
                    System.out.println("Enter ID of project");
                    idProject = inCommand.nextLine();
                    taskService.read(idProject);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.TR):
                case (CommandForProjectManager.TASK_REMOVE):
                    System.out.println("[TASK REMOVE]");
                    System.out.println("Enter the ID of the task you want to delete");
                    idTask = inCommand.nextLine();
                    taskService.delete(idTask);
                    System.out.println("[OK]");
                    break;
                case (CommandForProjectManager.TCL):
                case (CommandForProjectManager.TASK_CLEAR):
                    System.out.println("[TASK CLEAR]");
                    System.out.println("Enter ID of project");
                    idProject = inCommand.nextLine();
                    taskService.clear(idProject);
                    System.out.println("[OK]");
                    break;
            }
        } while (!command.equals("exit"));
    }
}