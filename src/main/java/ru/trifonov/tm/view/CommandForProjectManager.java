package ru.trifonov.tm.view;

class CommandForProjectManager {

    static final String HELP = "help";
    static final String H = "h";


//    Project command

    static final String PROJECT_CREATE = "project-create";
    static final String PC = "pc";
    static final String PROJECT_UPDATE = "project-update";
    static final String PU = "pu";
    static final String PROJECT_LIST = "project-list";
    static final String PL = "pl";
    static final String PROJECT_REMOVE = "project-remove";
    static final String PR = "pr";
    static final String PROJECT_CLEAR = "project-clear";
    static final String PCL = "pcl";

//    Task command

    static final String TASK_CREATE = "task-create";
    static final String TC = "tc";
    static final String TASK_UPDATE = "task-update";
    static final String TU = "tu";
    static final String TASK_LIST = "task-list";
    static final String TL = "tl";
    static final String TASK_REMOVE = "task-remove";
    static final String TR = "tr";
    static final String TASK_CLEAR = "taskClear";
    static final String TCL = "tcl";
}
