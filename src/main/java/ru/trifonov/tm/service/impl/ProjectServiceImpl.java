package ru.trifonov.tm.service.impl;

import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.UUID;

public class ProjectServiceImpl implements ProjectService {
    private ArrayList<Project> projects = new ArrayList<>();

    /*CRUD methods*/

    @Override
    public void create(String name, String id) {
        Project project = new Project();
        project.setName(name);
        project.setId(id);
        projects.add(project);
    }

    @Override
    public void update(String name, String id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId().equals(id)) projects.get(i).setName(name);
        }
    }

    @Override
    public void read() {
        for (Project project : projects) {
            System.out.println("ID  " + project.getId() + "  NAME  " + project.getName());
        }
    }

    @Override
    public void delete(String id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId().equals(id)) projects.remove(i);
        }
    }

    @Override
    public void clear() {
        projects.removeAll(projects);
    }

    @Override
    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
