package ru.trifonov.tm.service.impl;

import ru.trifonov.tm.model.Task;
import ru.trifonov.tm.service.TaskService;
import java.util.ArrayList;
import java.util.UUID;

public class TaskServiceImpl implements TaskService {
    private ArrayList<Task> tasks = new ArrayList<>();

    @Override
    public void create(String name, String id, String idProject) {
        Task task = new Task();
        task.setName(name);
        task.setIdTask(id);
        task.setIdProject(idProject);
        tasks.add(task);
    }

    @Override
    public void update(String name, String id) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getIdTask().equals(id)) tasks.get(i).setName(name);
        }
    }

    @Override
    public void read(String idProject) {
        for (Task task : tasks) {
            if (task.getIdProject().equals(idProject)) System.out.println("ID  " + task.getIdTask() + "  NAME  " + task.getName());
        }
    }

    @Override
    public void delete(String id) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getIdTask().equals(id)) tasks.remove(i);
        }
    }

    @Override
    public void clear(String idProject) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getIdProject().equals(idProject)) tasks.remove(i);
        }
    }

    @Override
    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
