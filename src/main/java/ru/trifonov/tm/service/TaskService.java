package ru.trifonov.tm.service;

public interface TaskService {
    void create(String name, String id, String idProject);
    void update(String name, String id);
    void read(String idProject);
    void delete(String id);
    void clear(String idProject);
    String generateRandomBasedUUID();
}
