package ru.trifonov.tm.service;

public interface ProjectService {
    void create(String name, String id);
    void update(String name, String id);
    void read();
    void delete(String id);
    void clear();
    String generateRandomBasedUUID();
}
